# remark-linkify-ssb-feeds

Given markdown text that has SSB feed ids in the text, this plugin converts all those ids into cypherlinks of the type `[@feedidfeedid=.ed25519](@feedidfeedid=.ed25519)`.

## Installation

```sh
npm install remark-linkify-ssb-feeds
```

## Usage

```js
// ...
var linkifySsbFeeds = require('remark-linkify-ssb-feeds');

unified()
  .use(parse)
  .use(linkifySsbFeeds)
// ...
```

Markdown document:

```
# Title

![funny.gif](&P1SGJj88qtwTgwuVyxhYzXXaw+xeyMP2LzXMZfkjbJ0=.sha256)

This is my friend: @6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519

This is redundantly linked: [@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519](@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519)

cc [@alreadyLinkedFriend](@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519)
```

Input AST:

```
root[5] (1:1-11:1, 0-373)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:67, 10-76)
│  └─ image (4:1-4:67, 10-76) [url="&P1SGJj88qtwTgwuVyxhYzXXaw+xeyMP2LzXMZfkjbJ0=.sha256"][alt="funny.gif"]
├─ paragraph[1] (6:1-6:73, 78-150)
│  └─ text: "This is my friend: @6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519" (6:1-6:73, 78-150)
├─ paragraph[2] (8:1-8:139, 152-290)
│  ├─ text: "This is redundantly linked: " (8:1-8:29, 152-180)
│  └─ link[1] (8:29-8:139, 180-290) [url="@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519"]
│     └─ text: "@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519" (8:30-8:83, 181-234)
└─ paragraph[2] (10:1-10:81, 292-372)
   ├─ text: "cc " (10:1-10:4, 292-295)
   └─ link[1] (10:4-10:81, 295-372) [url="@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519"]
      └─ text: "@alreadyLinkedFriend" (10:5-10:25, 296-316)
```

Output AST:

```
root[5] (1:1-11:1, 0-373)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title"
├─ paragraph[1] (4:1-4:67, 10-76)
│  └─ image (4:1-4:67, 10-76) [url="&P1SGJj88qtwTgwuVyxhYzXXaw+xeyMP2LzXMZfkjbJ0=.sha256"][alt="funny.gif"]
├─ paragraph[2] (6:1-6:73, 78-150)
│  ├─ text: "This is my friend: "
│  └─ link[1] [url="@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519"]
│     └─ text: "@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519"
├─ paragraph[2] (8:1-8:139, 152-290)
│  ├─ text: "This is redundantly linked: "
│  └─ link[1] (8:29-8:139, 180-290) [url="@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519"]
│     └─ text: "@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519" (8:30-8:83, 181-234)
└─ paragraph[2] (10:1-10:81, 292-372)
   ├─ text: "cc "
   └─ link[1] (10:4-10:81, 295-372) [url="@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519"]
      └─ text: "@alreadyLinkedFriend" (10:5-10:25, 296-316)
```

## License

[MIT](LICENSE)