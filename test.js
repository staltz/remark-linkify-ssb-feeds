const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const linkifySsbFeeds = require('./index');

test('it lifts a nested list to the root level', t => {
  t.plan(2);

  const markdown = `
# Title

![funny.gif](&P1SGJj88qtwTgwuVyxhYzXXaw+xeyMP2LzXMZfkjbJ0=.sha256)

This is my friend: @6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519

This is redundantly linked: [@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519](@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519)

cc [@alreadyLinkedFriend](@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519)
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'image',
            title: null,
            url: '&P1SGJj88qtwTgwuVyxhYzXXaw+xeyMP2LzXMZfkjbJ0=.sha256',
            alt: 'funny.gif',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 67, offset: 76},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 67, offset: 76},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value:
              'This is my friend: @6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
            position: {
              start: {line: 6, column: 1, offset: 78},
              end: {line: 6, column: 73, offset: 150},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 78},
          end: {line: 6, column: 73, offset: 150},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'This is redundantly linked: ',
            position: {
              start: {line: 8, column: 1, offset: 152},
              end: {line: 8, column: 29, offset: 180},
              indent: [],
            },
          },
          {
            type: 'link',
            title: null,
            url: '@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
            children: [
              {
                type: 'text',
                value: '@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
                position: {
                  start: {line: 8, column: 30, offset: 181},
                  end: {line: 8, column: 83, offset: 234},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 8, column: 29, offset: 180},
              end: {line: 8, column: 139, offset: 290},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 8, column: 1, offset: 152},
          end: {line: 8, column: 139, offset: 290},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'cc ',
            position: {
              start: {line: 10, column: 1, offset: 292},
              end: {line: 10, column: 4, offset: 295},
              indent: [],
            },
          },
          {
            type: 'link',
            title: null,
            url: '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519',
            children: [
              {
                type: 'text',
                value: '@alreadyLinkedFriend',
                position: {
                  start: {line: 10, column: 5, offset: 296},
                  end: {line: 10, column: 25, offset: 316},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 10, column: 4, offset: 295},
              end: {line: 10, column: 81, offset: 372},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 10, column: 1, offset: 292},
          end: {line: 10, column: 81, offset: 372},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 11, column: 1, offset: 373},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = linkifySsbFeeds()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [{type: 'text', value: 'Title'}],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'image',
            title: null,
            url: '&P1SGJj88qtwTgwuVyxhYzXXaw+xeyMP2LzXMZfkjbJ0=.sha256',
            alt: 'funny.gif',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 67, offset: 76},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 67, offset: 76},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {type: 'text', value: 'This is my friend: '},
          {
            type: 'link',
            title: null,
            url: '@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
            children: [
              {
                type: 'text',
                value: '@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
              },
            ],
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 78},
          end: {line: 6, column: 73, offset: 150},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {type: 'text', value: 'This is redundantly linked: '},
          {
            type: 'link',
            title: null,
            url: '@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
            children: [
              {
                type: 'text',
                value: '@6ilZq3kN0F+dXFHAPjAwMm87JEb/VdB+LC9eIMW3sa0=.ed25519',
                position: {
                  start: {line: 8, column: 30, offset: 181},
                  end: {line: 8, column: 83, offset: 234},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 8, column: 29, offset: 180},
              end: {line: 8, column: 139, offset: 290},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 8, column: 1, offset: 152},
          end: {line: 8, column: 139, offset: 290},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {type: 'text', value: 'cc '},
          {
            type: 'link',
            title: null,
            url: '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519',
            children: [
              {
                type: 'text',
                value: '@alreadyLinkedFriend',
                position: {
                  start: {line: 10, column: 5, offset: 296},
                  end: {line: 10, column: 25, offset: 316},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 10, column: 4, offset: 295},
              end: {line: 10, column: 81, offset: 372},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 10, column: 1, offset: 292},
          end: {line: 10, column: 81, offset: 372},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 11, column: 1, offset: 373},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});
